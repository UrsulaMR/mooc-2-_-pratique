# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien  

_une phrase en italique_  

** Une phrase en gras**  

Un lien vers www.fun.mooc.fr  

Une ligne de 
```python
    def code() 
``` 
    
## Sous-partie 2 : listes 
Liste à puces  

* item
    * Sub-item sous item
    * Sub-item sous item
* item
* item

Liste numérotée
1. item
2. item
3. item

## Sous-partie 3 : listes
<code> # Extrait de code  
