## Exemple de méta-données de fiche prof pour une activité

**Thématique :** Module Internet 

**Notions liées :** adresse IP, protocole TCP/IP, datagramme, routage, routeur, réseau local, masque réseau, Internet. 

**Résumé de l’activité :** simuler la création d'un réseau avec le logiciel filius pour manipuler concrétement les objets numériques expliqués.

**Objectifs :** mettre en pratique un cas d'usage d'installation réseau, faire passer des notions à la manipulation.

**Auteur :** David Roche <"dav74130" <dav74130@gmail.com>; >

**Durée de l’activité :** une heure (après la prise de connaissance du cours).

**Forme de participation :** individuelle ou en binôme, en autonomie, sur machine.

**Matériel nécessaire :** ordinateur avec installé le logiciel filius

**Préparation :** il est recommandé de tester le lancement du logiciel sur les machines du lycée avant la séance.

### Références:

**Fiche élève cours :** https://pixees.fr/informatiquelycee/sec/s1_1.html

**Fiche élève activité :** https://pixees.fr/informatiquelycee/sec/s1_4.html

**Déroulé :** [deroule-snt-module-internet-simulation.md](deroule-snt-module-internet-simulation.md)


