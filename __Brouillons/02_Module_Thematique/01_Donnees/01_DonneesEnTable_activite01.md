# Données en tables

## Activité 1 : Découvrir les formats CSV et TSV

### Contexte et pré-requis

Vous avez une séance de TP (devant machines). Les élèves ont déjà rencontré la notion de fichiers textes et les formats CSV et TSV ont été présentés dans une séance de cours. Mais ils n'ont encore jamais manipulé : ni les fichiers avec Python, ni bien sûr les formats en question.

### Exercice 1 : Analyse de la future activité à créer

1. Quels sont les connaissances et les compétences visées par l'activité ?
2. A quel niveaux d'enseignement cette activité s'adresse-t-elle ?
3. Quels sont les pré-requis à l'activité ?
4. Quel matériel (y compris logiciel) vous semble nécessaire à cette activité ?


### Exercice 2 : Création d'une séance

Proposer une suite d'exercices, pauses discussion pour une séance de 2h qui doit se terminer par une petite évaluation formative de type QCM. 


### Exercice 3 : Gérer les imprévus

- Certains élèves vous demandent l'intérêt de faire les manipulations avec le langage Python puisqu'on peut ouvrir un fichier CSV avec un tableur. Quelle réponse leur apportez-vous ?
- Certains élèves ne comprennent pas le problème potentiel avec le séparateur virgule. Quelle aide proposez-vous ?
- Quelques élèves semblent avancer très vite, que faire pour les _occuper_ ou les _ralentir_ ?
