**Exemple de méta-données de fiche _prof_ pour une activité**

# Titre de l'activité

- **Fiche élève cours :**
- **Fiche élève activité :**
- **Déroulé :** _si déjà connu_

--- 

**Thématique :** 

**Notions liées :** 

**Résumé de l’activité :** 

**Objectifs :** 

**Auteur :** 

**Durée de l’activité :** 

**Forme de participation :** 

**Matériel nécessaire :** 

**Préparation :** 

**Autres références :** (_facultatif_)




